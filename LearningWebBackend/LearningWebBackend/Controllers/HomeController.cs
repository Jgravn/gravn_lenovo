﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace LearningWebBackend.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            string name = "Jesper";

            DateTime birthdate = new DateTime(1992, 04, 16);

            int age = DateTime.Today.Year - birthdate.Year;

            ViewData["name"] = name;
            ViewData["age"] = age;
            ViewData["birthday"] = birthdate.Day;
            ViewData["birthmonth"] = birthdate.Month;
            ViewData["birthyear"] = birthdate.Year;

            return View();
        }

        public IActionResult About()
        {
            return View();
        }
    }
}