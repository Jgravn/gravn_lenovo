﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace LearningWebBackend.Controllers
{
    public class TimeCalculatorController : Controller
    {
        public IActionResult Index()
        {

            return View();
        }

        public int tal()
        {
            return 5;
        }

        [HttpPost]
        public IActionResult Index(IFormCollection formCollection)
        {
            int hours = Convert.ToInt32(formCollection["Hours"]);
            int minutes = Convert.ToInt32(formCollection["Minutes"]);
            int seconds = Convert.ToInt32(formCollection["Seconds"]);
            TimeSpan ts = new TimeSpan(0, hours, minutes, seconds);
            double total = ts.TotalSeconds;

            ViewData["Hours"] = hours;
            ViewData["Minutes"] = minutes;
            ViewData["Seconds"] = seconds;

            ViewData["TotalTime"] = ts.TotalMinutes;
            return View();
        }

        [HttpPost]
        public IActionResult Restart(IFormCollection formCollection)
        {
            ViewData["TotalTime"] = null;
            Index();
            return View();
        }
    }
}